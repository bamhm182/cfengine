# Summary

- [CFEngine](./index.md)
- [Installation](./installation/index.md)
  - [Ubuntu](./installation/ubuntu/index.md)
    - [Desktop](./installation/ubuntu/desktop.md)
    - [Server](./installation/ubuntu/server.md)
- [Configuration](./configuration/index.md)
  - [Initial Setup](./configuration/initial-setup.md)
- [Commands](./commands/index.md)
  - [cf-agent](./commands/cf-agent.md)
  - [cf-key](./commands/cf-key.md)
