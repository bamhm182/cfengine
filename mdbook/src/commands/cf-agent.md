# cf-agent

`cf-agent` is the "instigator of change", as described in the CFEngine documentation. `cf-agent` evaluates policies and acts on them, making any necessary changes to the system. It is either run manually by the user or by `cf-execd` or `cf-serverd`. If necessary, it is able to start `cf-execd`, `cf-serverd`, and `cf-monitord` if necessary. It also uses `cf-promises` to validate promises before executing them.

When run without the `-f` argument, `cf-agent` will try to run one of the following promise files:
* Run as `root`: `/var/cfengine/inputs/promises.cf` 
* Run as `user`: `~/.cfagent/inputs/promises.cf`

If it is unable to run the above command, it will try to run `inputs/failsafe.cf`, which is intended to be a barebones file that attempts to restore CFEngine to working condition.

## Examples

> Bootstrap
> ```
> /var/cfengine/bin/cf-agent --bootstrap ${PolicyHubIP}
> ```
>> If Ran on Policy Hub
>> ```
>> R: Bootstrapping from host '192.168.174.134' via built-in policy '/var/cfengine/inputs/failsafe.cf'
>> R: This host assumes the role of policy server
>> R: Updated local policy from policy server
>> R: Triggered an initial run of the policy
>>   notice: Bootstrap to '192.168.174.134' completed successfully!
>> ```
>> If Ran on Client
>> ```
>>   notice: Bootstrap mode: implicitly trusting server, use --trust-server=no if server trust is already established
>>   notice: Q: ".../cf-agent" --de": R: Hello world!
>> R: Bootstrapping from host '192.168.174.134' via built-in policy '/var/cfengine/inputs/failsafe.cf'
>> R: This autonomous node assumes the role of voluntary client
>> R: Updated local policy from policy server
>> R: Triggered an initial run of the policy
>>   notice: Bootstrap to '192.168.174.134' completed successfully!
>> ```

> Check Version
> ```
> /var/cfengine/bin/cf-agent --version
> ```
>> ```
>> CFEngine Core 3.15.1
>> ```

