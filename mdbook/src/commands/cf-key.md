# cf-key

`cf-key` is used to create a cryptographic key pair for the current host, which is used when communicating with the policy hub, or any other CFEngine server.

## Examples

> Generate Keys
> ```
> /var/cfengine/bin/cf-key
> ```
>> Success
>> ```
>> Making a key pair for cfengine, please wait, this could take a minute...
>> ```
>> Keys Already Exist
>> ```
>> error: A key file already exists at /var/cfengine/ppkeys/localhost.pub
>> ```


> Regenerate Keys
> ```
> rm -f /var/cfengine/ppkeys/localhost.*
> /var/cfengine/bin/cf-key
> ```
>> ```
>> Making a key pair for cfengine, please wait, this could take a minute...
>> ```
