# Commands

![](../img/component-dependencies.png)

The above image depicts various binaries included with CFEngine and how they all tie together.
* Dotted Lines: Binaries that start each other
* Solid Lines: Communication
* Red: Data Flow
