# Ubuntu Desktop Installation

## Quick and Easy

They have a setup script you can run that adds their repo and keys to your setup, then installs it via apt. I've tested it on a baseline 18.04.4 Desktop and it works fine.

> ```
> wget -O- https://s3.amazonaws.com/cfengine.packages/quick-install-cfengine-community.sh | sudo bash
> ```

## Manual Installation
