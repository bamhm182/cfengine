# Initial Setup

## Keys

> Generate Keys
> ```
> /var/cfengine/bin/cf-key
> ```
> ```
> Making a key pair for cfengine, please wait, this could take a minute...
> ```

If you get the following message, keys are already generated:

> ```
> A key file already exists at /var/cfengine/ppkeys/localhost.pub
> ```

If you want to delete the pre-generated keys and create new ones, you can do the following:

> Regenerate Keys
> ```
> rm /var/cfengine/ppkeys/*
> /var/cfengine/bin/cf-key
> ```
> ```
> Making a key pair for cfengine, please wait, this could take a minute...
> ```

## Bootstrap

Once it's installed, you need to bootstrap it to the Policy Hub. You can do that with the following command:

> Bootstrap
> ```
> /var/cfengine/bin/cf-engine --bootstrap ${PolicyHubIP}
> ```

Note, if you are configuring the current workstation to be the Policy Hub, you can put in the IP address of your current computer.

